#include "Status.h"

bool Status::SetLv(int i)
{
	if (100 <= i)
	{
		i = 99;
	}

	else if (i <= 0)
	{
		return false;
	}

	lv = i;
	return true;
}

void Status::Calc()
{
	hp = lv * lv + 50;
	atk = lv * 10;
	def = lv * 9;
}

int Status::Gethp()
{
	return hp;
}

int Status::Getatk()
{
	return atk;
}

int Status::Getdef()
{
	return def;
}
