#include"Player.h"
#include"Enemy.h"
#include<iostream>

int main()
{
	Player p;
	Enemy e;

	int damege;

	for (int turn = 1;; turn++)
	{
		std::cout << "\n======" << turn << "ターン目======\n";

		p.DispHp();
		e.DispHp();

		damege = p.Attack(e.GetDef());
		e.Damege(damege);

		if (e.IsDead())
		{
			break;
		}

		damege = e.Attack(p.GetDef());

		p.Damege(damege);

		if (p.IsDead())
		{
			break;
		}
	}
	std::cout << "終了\n";
}