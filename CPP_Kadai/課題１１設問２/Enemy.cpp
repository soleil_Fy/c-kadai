#include"Enemy.h"
#include<iostream>

Enemy::Enemy()
{
	hp = 200;
	atk = 35;
	def = 40;
}

void Enemy::DispHp()
{
	std::cout << "プレイヤーHP＝" << hp << "\n";
}

int Enemy::Attack(int i)
{
	std::cout << ("プレイヤーの攻撃！　");
	return atk - i / 2;
}

void Enemy::Damege(int i)
{
	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

int Enemy::GetDef()
{
	return def;
}

bool Enemy::IsDead()
{
	if (hp < 0)
	{
		return true;
	}
	return false;
}